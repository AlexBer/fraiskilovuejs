import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import Toasted from 'vue-toasted';
 
Vue.use(Toasted)
Vue.toasted.register('error', message => message,  {
  theme: "outline", 
  position : 'bottom-center',
  duration : 2500,
  type: 'error'
}),
Vue.toasted.register('success', message => message,  {
  theme: "outline", 
  position : 'bottom-center',
  duration : 2500,
  type: 'success'
})

Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
