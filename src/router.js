import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'
import About from './views/About.vue'
import Trips from './views/Trips.vue'
import Cars from './views/Cars.vue'
import AddCar from './views/AddCar.vue'
import AddTrip from './views/AddTrip.vue'
import EditCar from './views/EditCar.vue'
import EditTrip from './views/EditTrip.vue'
import Login from './views/Login.vue'

Vue.use(Router)

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/about',
      name: 'about',
      component: About
    },
    {
      path: '/trips',
      name: 'trips',
      component: Trips
    },
    {
      path: '/cars',
      name: 'cars',
      component: Cars
    },
    {
      path: '/addcar',
      name: 'addcar',
      component: AddCar
    },
    {
      path: '/addtrip',
      name: 'addtrip',
      component: AddTrip
    },
    {
      path: '/editcar',
      name: 'editcar',
      component: EditCar
    },
    {
      path: '/edittrip',
      name: 'edittrip',
      component: EditTrip
    },
    {
      path: '/login',
      name: 'login',
      component: Login
    }
  ]
})
