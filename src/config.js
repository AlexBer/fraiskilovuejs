export const getHeaders = function() {
  const token = JSON.parse(localStorage.access_token)
  const headers = {
  'Accept': 'application/json',
  'Authorization': ' Bearer ' + token
  }
  return headers
}